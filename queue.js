let collection = [];

// Write the queue functions below.

function print(){
	for (let x = 1; x < collection.length; x++) {
    let y = x - 1
    let tempCol = collection[i]
    while (y >= 0 && collection[y] > tempCol) {
      collection[y + 1] = collection[y]
      y--
    }
    collection[y+1] = tempCol
  }
  return collection
}

function enqueue(element){
	collection[collection.length] = element;
	return collection	
}

/*

function enqueue(element){
	collection.push(element)
	return collection	
}

*/

function dequeue(element, index){
	collection.shift(element)
	return collection
}

function front(){
	return collection[0]
}

function size(){
	return collection.length
}

function isEmpty(){
	if (collection.length == 0) {
		return true
	} else {
		return false
	}
}

module.exports = {
	collection,
	print,
	enqueue, 
	dequeue,
	front,
	size, 
	isEmpty


};